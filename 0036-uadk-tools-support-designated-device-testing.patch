From 829bc553310349ee7c654397204e8b348d7610f4 Mon Sep 17 00:00:00 2001
From: Yang Shen <shenyang39@huawei.com>
Date: Mon, 11 Mar 2024 16:27:04 +0800
Subject: [PATCH 36/44] uadk/tools - support designated device testing

Add a parameter 'device' to designate a device. The input should
use the device name from '/sys/class/uacce/<name>'. Only full
matching device names are supported.

Signed-off-by: Yang Shen <shenyang39@huawei.com>
---
 uadk_tool/benchmark/hpre_uadk_benchmark.c | 86 ++++++++++++++-------
 uadk_tool/benchmark/sec_uadk_benchmark.c  | 86 ++++++++++++++-------
 uadk_tool/benchmark/uadk_benchmark.c      | 64 ++++++++-------
 uadk_tool/benchmark/uadk_benchmark.h      | 36 +++++----
 uadk_tool/benchmark/zip_uadk_benchmark.c  | 94 ++++++++++++++++-------
 5 files changed, 237 insertions(+), 129 deletions(-)

diff --git a/uadk_tool/benchmark/hpre_uadk_benchmark.c b/uadk_tool/benchmark/hpre_uadk_benchmark.c
index 0cbbdf2..729728f 100644
--- a/uadk_tool/benchmark/hpre_uadk_benchmark.c
+++ b/uadk_tool/benchmark/hpre_uadk_benchmark.c
@@ -346,43 +346,66 @@ static int hpre_uadk_param_parse(thread_data *tddata, struct acc_option *options
 
 static int init_hpre_ctx_config(struct acc_option *options)
 {
+	struct uacce_dev_list *list, *tmp;
 	int subtype = options->subtype;
 	char *alg = options->algclass;
 	int mode = options->syncmode;
+	struct uacce_dev *dev = NULL;
 	struct sched_params param;
-	struct uacce_dev *dev;
-	int max_node;
+	int max_node, i;
+	char *dev_name;
 	int ret = 0;
-	int i = 0;
 
 	max_node = numa_max_node() + 1;
 	if (max_node <= 0)
 		return -EINVAL;
 
-	memset(&g_ctx_cfg, 0, sizeof(struct wd_ctx_config));
-	g_ctx_cfg.ctx_num = g_ctxnum;
-	g_ctx_cfg.ctxs = calloc(g_ctxnum, sizeof(struct wd_ctx));
-	if (!g_ctx_cfg.ctxs)
-		return -ENOMEM;
+	list = wd_get_accel_list(alg);
+	if (!list) {
+		HPRE_TST_PRT("failed to get %s device\n", alg);
+		return -ENODEV;
+	}
 
-	while (i < g_ctxnum) {
-		dev = wd_get_accel_dev(alg);
-		if (!dev) {
-			HPRE_TST_PRT("failed to get %s device\n", alg);
-			ret = -EINVAL;
-			goto out;
+	if (strlen(options->device) == 0) {
+		dev = list->dev;
+	} else {
+		for (tmp = list; tmp; tmp = tmp->next) {
+			dev_name = strrchr(tmp->dev->dev_root, '/') + 1;
+			if (!strcmp(dev_name, options->device)) {
+				dev = tmp->dev;
+				break;
+			}
 		}
+	}
 
-		for (; i < g_ctxnum; i++) {
-			g_ctx_cfg.ctxs[i].ctx = wd_request_ctx(dev);
-			if (!g_ctx_cfg.ctxs[i].ctx)
-				break;
+	if (dev == NULL) {
+		HPRE_TST_PRT("failed to find device %s\n", options->device);
+		ret = -ENODEV;
+		goto free_list;
+	}
+
+	/* If there is no numa, we defualt config to zero */
+	if (dev->numa_id < 0)
+		dev->numa_id = 0;
 
-			g_ctx_cfg.ctxs[i].op_type = 0; // default op_type
-			g_ctx_cfg.ctxs[i].ctx_mode = (__u8)mode;
+	memset(&g_ctx_cfg, 0, sizeof(struct wd_ctx_config));
+	g_ctx_cfg.ctx_num = g_ctxnum;
+	g_ctx_cfg.ctxs = calloc(g_ctxnum, sizeof(struct wd_ctx));
+	if (!g_ctx_cfg.ctxs) {
+		ret = -ENOMEM;
+		goto free_list;
+	}
+
+	for (i = 0; i < g_ctxnum; i++) {
+		g_ctx_cfg.ctxs[i].ctx = wd_request_ctx(dev);
+		if (!g_ctx_cfg.ctxs[i].ctx) {
+			HPRE_TST_PRT("failed to alloc %dth ctx\n", i);
+			ret = -ENODEV;
+			goto free_ctx;
 		}
 
-		free(dev);
+		g_ctx_cfg.ctxs[i].op_type = 0;
+		g_ctx_cfg.ctxs[i].ctx_mode = (__u8)mode;
 	}
 
 	switch(subtype) {
@@ -401,11 +424,11 @@ static int init_hpre_ctx_config(struct acc_option *options)
 		break;
 	default:
 		HPRE_TST_PRT("failed to parse alg subtype!\n");
-		return -EINVAL;
+		goto free_ctx;
 	}
 	if (!g_sched) {
 		HPRE_TST_PRT("failed to alloc sched!\n");
-		goto out;
+		goto free_ctx;
 	}
 
 	g_sched->name = SCHED_SINGLE;
@@ -417,7 +440,7 @@ static int init_hpre_ctx_config(struct acc_option *options)
 	ret = wd_sched_rr_instance(g_sched, &param);
 	if (ret) {
 		HPRE_TST_PRT("failed to fill hpre sched data!\n");
-		goto out;
+		goto free_sched;
 	}
 
 	/* init */
@@ -438,17 +461,22 @@ static int init_hpre_ctx_config(struct acc_option *options)
 	}
 	if (ret) {
 		HPRE_TST_PRT("failed to get hpre ctx!\n");
-		goto out;
+		goto free_sched;
 	}
 
 	return 0;
-out:
-	for (i = i - 1; i >= 0; i--)
-		wd_release_ctx(g_ctx_cfg.ctxs[i].ctx);
 
-	free(g_ctx_cfg.ctxs);
+free_sched:
 	wd_sched_rr_release(g_sched);
 
+free_ctx:
+	for (; i >= 0; i--)
+		wd_release_ctx(g_ctx_cfg.ctxs[i].ctx);
+	free(g_ctx_cfg.ctxs);
+
+free_list:
+	wd_free_list_accels(list);
+
 	return ret;
 }
 
diff --git a/uadk_tool/benchmark/sec_uadk_benchmark.c b/uadk_tool/benchmark/sec_uadk_benchmark.c
index 92e967a..105fb1a 100644
--- a/uadk_tool/benchmark/sec_uadk_benchmark.c
+++ b/uadk_tool/benchmark/sec_uadk_benchmark.c
@@ -516,42 +516,66 @@ static int sec_uadk_param_parse(thread_data *tddata, struct acc_option *options)
 
 static int init_ctx_config(struct acc_option *options)
 {
+	struct uacce_dev_list *list, *tmp;
 	struct sched_params param = {0};
-	struct uacce_dev *dev = NULL;
-	char *alg = options->algclass;
 	int subtype = options->subtype;
+	char *alg = options->algclass;
 	int mode = options->syncmode;
-	int max_node = 0;
+	struct uacce_dev *dev = NULL;
+	int max_node, i;
+	char *dev_name;
 	int ret = 0;
-	int i = 0;
 
 	max_node = numa_max_node() + 1;
 	if (max_node <= 0)
 		return -EINVAL;
 
-	memset(&g_ctx_cfg, 0, sizeof(struct wd_ctx_config));
-	g_ctx_cfg.ctx_num = g_ctxnum;
-	g_ctx_cfg.ctxs = calloc(g_ctxnum, sizeof(struct wd_ctx));
-	if (!g_ctx_cfg.ctxs)
-		return -ENOMEM;
+	list = wd_get_accel_list(alg);
+	if (!list) {
+		SEC_TST_PRT("failed to get %s device\n", alg);
+		return -ENODEV;
+	}
 
-	while (i < g_ctxnum) {
-		dev = wd_get_accel_dev(alg);
-		if (!dev) {
-			SEC_TST_PRT("failed to get %s device\n", alg);
-			goto out;
+	if (strlen(options->device) == 0) {
+		dev = list->dev;
+	} else {
+		for (tmp = list; tmp; tmp = tmp->next) {
+			dev_name = strrchr(tmp->dev->dev_root, '/') + 1;
+			if (!strcmp(dev_name, options->device)) {
+				dev = tmp->dev;
+				break;
+			}
 		}
+	}
 
-		for (; i < g_ctxnum; i++) {
-			g_ctx_cfg.ctxs[i].ctx = wd_request_ctx(dev);
-			if (!g_ctx_cfg.ctxs[i].ctx)
-				break;
+	if (dev == NULL) {
+		SEC_TST_PRT("failed to find device %s\n", options->device);
+		ret = -ENODEV;
+		goto free_list;
+	}
+
+	/* If there is no numa, we defualt config to zero */
+	if (dev->numa_id < 0)
+		dev->numa_id = 0;
+
+	memset(&g_ctx_cfg, 0, sizeof(struct wd_ctx_config));
+	g_ctx_cfg.ctx_num = g_ctxnum;
+	g_ctx_cfg.ctxs = calloc(g_ctxnum, sizeof(struct wd_ctx));
+	if (!g_ctx_cfg.ctxs) {
+		ret = -ENOMEM;
+		goto free_list;
+	}
 
-			g_ctx_cfg.ctxs[i].op_type = 0; // default op_type
-			g_ctx_cfg.ctxs[i].ctx_mode = (__u8)mode;
+	for (i = 0; i < g_ctxnum; i++) {
+		g_ctx_cfg.ctxs[i].ctx = wd_request_ctx(dev);
+		if (!g_ctx_cfg.ctxs[i].ctx) {
+			SEC_TST_PRT("failed to alloc %dth ctx\n", i);
+			ret = -ENOMEM;
+			goto free_ctx;
 		}
 
-		free(dev);
+		g_ctx_cfg.ctxs[i].op_type = 0;
+		g_ctx_cfg.ctxs[i].ctx_mode = (__u8)mode;
 	}
 
 	switch(subtype) {
@@ -566,11 +590,11 @@ static int init_ctx_config(struct acc_option *options)
 		break;
 	default:
 		SEC_TST_PRT("failed to parse alg subtype!\n");
-		return -EINVAL;
+		goto free_ctx;
 	}
 	if (!g_sched) {
 		SEC_TST_PRT("failed to alloc sched!\n");
-		goto out;
+		goto free_ctx;
 	}
 
 	g_sched->name = SCHED_SINGLE;
@@ -582,7 +606,7 @@ static int init_ctx_config(struct acc_option *options)
 	ret = wd_sched_rr_instance(g_sched, &param);
 	if (ret) {
 		SEC_TST_PRT("failed to fill sched data!\n");
-		goto out;
+		goto free_sched;
 	}
 
 	/* init */
@@ -599,17 +623,21 @@ static int init_ctx_config(struct acc_option *options)
 	}
 	if (ret) {
 		SEC_TST_PRT("failed to cipher ctx!\n");
-		goto out;
+		goto free_sched;
 	}
 
 	return 0;
 
-out:
-	for (i--; i >= 0; i--)
-		wd_release_ctx(g_ctx_cfg.ctxs[i].ctx);
+free_sched:
+	wd_sched_rr_release(g_sched);
 
+free_ctx:
+	for (; i >= 0; i--)
+		wd_release_ctx(g_ctx_cfg.ctxs[i].ctx);
 	free(g_ctx_cfg.ctxs);
-	wd_sched_rr_release(g_sched);
+
+free_list:
+	wd_free_list_accels(list);
 
 	return ret;
 }
diff --git a/uadk_tool/benchmark/uadk_benchmark.c b/uadk_tool/benchmark/uadk_benchmark.c
index 0ebbb68..5dbe26a 100644
--- a/uadk_tool/benchmark/uadk_benchmark.c
+++ b/uadk_tool/benchmark/uadk_benchmark.c
@@ -491,6 +491,7 @@ static void parse_alg_param(struct acc_option *option)
 void cal_perfermance_data(struct acc_option *option, u32 sttime)
 {
 	u8 palgname[MAX_ALG_NAME];
+	char *unit = "KiB/s";
 	double perfermance;
 	double cpu_rate;
 	u32 ttime = 1000;
@@ -506,8 +507,8 @@ void cal_perfermance_data(struct acc_option *option, u32 sttime)
 		if (option->syncmode == SYNC_MODE) {
 			if (get_recv_time() == option->threads)
 				break;
-		} else { // ASYNC_MODE
-			if (get_recv_time() == 1) // poll complete
+		} else {
+			if (get_recv_time() == 1)
 				break;
 		}
 		usleep(1000);
@@ -525,14 +526,17 @@ void cal_perfermance_data(struct acc_option *option, u32 sttime)
 		palgname[i] = '\0';
 
 	ptime = ptime - sttime;
+	cpu_rate = (double)ptime / option->times;
+
 	perfdata = g_recv_data.pkg_len * g_recv_data.recv_cnt / 1024.0;
-	perfops = (double)(g_recv_data.recv_cnt) / 1000.0;
 	perfermance = perfdata / option->times;
+
+	perfops = g_recv_data.recv_cnt / 1000.0;
 	ops = perfops / option->times;
-	cpu_rate = (double)ptime / option->times;
-	ACC_TST_PRT("algname:	length:		perf:		iops:		CPU_rate:\n"
-			"%s	%-2uBytes 	%.1fKB/s 	%.1fKops 	%.2f%%\n",
-			palgname, option->pktlen, perfermance, ops, cpu_rate);
+
+	ACC_TST_PRT("algname:\tlength:\t\tperf:\t\tiops:\t\tCPU_rate:\n"
+		    "%s\t%-2uBytes \t%.2f%s\t%.1fKops \t%.2f%%\n",
+		    palgname, option->pktlen, perfermance, unit, ops, cpu_rate);
 }
 
 static int benchmark_run(struct acc_option *option)
@@ -744,24 +748,25 @@ int acc_cmd_parse(int argc, char *argv[], struct acc_option *option)
 	int c;
 
 	static struct option long_options[] = {
-		{"help",      no_argument,       0, 0},
-		{"alg",       required_argument, 0, 1},
-		{"mode",      required_argument, 0, 2},
-		{"opt",       required_argument, 0, 3},
-		{"sync",      no_argument,       0, 4},
-		{"async",     no_argument,       0, 5},
-		{"pktlen",    required_argument, 0, 6},
-		{"seconds",   required_argument, 0, 7},
-		{"thread",    required_argument, 0, 8},
-		{"multi",     required_argument, 0, 9},
-		{"ctxnum",    required_argument, 0, 10},
-		{"prefetch",  no_argument,       0, 11},
-		{"engine",    required_argument, 0, 12},
-		{"alglist",   no_argument,       0, 13},
-		{"latency",   no_argument,       0, 14},
-		{"winsize",   required_argument, 0, 15},
-		{"complevel", required_argument, 0, 16},
-		{"init2", no_argument, 0, 17},
+		{"help",	no_argument,		0, 0},
+		{"alg",		required_argument,	0, 1},
+		{"mode",	required_argument,	0, 2},
+		{"opt",		required_argument,	0, 3},
+		{"sync",	no_argument,		0, 4},
+		{"async",	no_argument,		0, 5},
+		{"pktlen",	required_argument,	0, 6},
+		{"seconds",	required_argument,	0, 7},
+		{"thread",	required_argument,	0, 8},
+		{"multi",	required_argument,	0, 9},
+		{"ctxnum",	required_argument,	0, 10},
+		{"prefetch",	no_argument,		0, 11},
+		{"engine",	required_argument,	0, 12},
+		{"alglist",	no_argument,		0, 13},
+		{"latency",	no_argument,		0, 14},
+		{"winsize",	required_argument,	0, 15},
+		{"complevel",	required_argument,	0, 16},
+		{"init2",	no_argument,		0, 17},
+		{"device",	required_argument,	0, 18},
 		{0, 0, 0, 0}
 	};
 
@@ -826,8 +831,15 @@ int acc_cmd_parse(int argc, char *argv[], struct acc_option *option)
 		case 17:
 			option->inittype = INIT2_TYPE;
 			break;
+		case 18:
+			if (strlen(optarg) >= MAX_DEVICE_NAME) {
+				ACC_TST_PRT("invalid: device name is %s\n", optarg);
+				goto to_exit;
+			}
+			strcpy(option->device, optarg);
+			break;
 		default:
-			ACC_TST_PRT("bad input test parameter!\n");
+			ACC_TST_PRT("invalid: bad input parameter!\n");
 			print_help();
 			goto to_exit;
 		}
diff --git a/uadk_tool/benchmark/uadk_benchmark.h b/uadk_tool/benchmark/uadk_benchmark.h
index 1752948..fd3ebe5 100644
--- a/uadk_tool/benchmark/uadk_benchmark.h
+++ b/uadk_tool/benchmark/uadk_benchmark.h
@@ -6,27 +6,28 @@
 #include <errno.h>
 #include <fcntl.h>
 #include <getopt.h>
+#include <linux/random.h>
 #include <pthread.h>
-#include <unistd.h>
 #include <stdbool.h>
+#include <string.h>
 #include <stdio.h>
 #include <stdlib.h>
-#include <string.h>
 #include <signal.h>
-#include <linux/random.h>
 #include <sys/syscall.h>
 #include <sys/time.h>
+#include <unistd.h>
 
-#define ACC_TST_PRT printf
-#define PROCESS_NUM	32
-#define THREADS_NUM	64
-#define MAX_CTX_NUM	64
+#define ACC_TST_PRT		printf
+#define PROCESS_NUM		32
+#define THREADS_NUM		64
+#define MAX_CTX_NUM		64
 #define MAX_TIME_SECONDS	128
-#define BYTES_TO_MB	20
-#define MAX_OPT_TYPE	6
-#define MAX_DATA_SIZE	(15 * 1024 * 1024)
-#define MAX_ALG_NAME 64
-#define ACC_QUEUE_SIZE	1024
+#define BYTES_TO_MB		20
+#define MAX_OPT_TYPE		6
+#define MAX_DATA_SIZE		(15 * 1024 * 1024)
+#define MAX_ALG_NAME		64
+#define ACC_QUEUE_SIZE		1024
+#define MAX_DEVICE_NAME		64
 
 #define MAX_BLOCK_NM		16384 /* BLOCK_NUM must 4 times of POOL_LENTH */
 #define MAX_POOL_LENTH		4096
@@ -35,15 +36,15 @@
 #define SEC_2_USEC		1000000
 #define HASH_ZISE		16
 
+#define SCHED_SINGLE		"sched_single"
+#define ARRAY_SIZE(x)		(sizeof(x) / sizeof((x)[0]))
+#define gettid()		syscall(__NR_gettid)
+
 typedef unsigned long long u64;
 typedef unsigned int u32;
 typedef unsigned short u16;
 typedef unsigned char u8;
 
-#define SCHED_SINGLE "sched_single"
-#define ARRAY_SIZE(x)		(sizeof(x) / sizeof((x)[0]))
-#define gettid() syscall(__NR_gettid)
-
 /**
  * struct acc_option - Define the test acc app option list.
  * @algclass: 0:cipher 1:digest
@@ -55,9 +56,10 @@ typedef unsigned char u8;
  * @latency: test packet running time
  */
 struct acc_option {
-	char algname[64];
+	char algname[MAX_ALG_NAME];
 	char algclass[64];
 	char engine[64];
+	char device[MAX_DEVICE_NAME];
 	u32 algtype;
 	u32 modetype;
 	u32 optype;
diff --git a/uadk_tool/benchmark/zip_uadk_benchmark.c b/uadk_tool/benchmark/zip_uadk_benchmark.c
index 9681c22..63fbdab 100644
--- a/uadk_tool/benchmark/zip_uadk_benchmark.c
+++ b/uadk_tool/benchmark/zip_uadk_benchmark.c
@@ -16,6 +16,7 @@
 #define MAX_POOL_LENTH_COMP		1
 #define COMPRESSION_RATIO_FACTOR	0.7
 #define CHUNK_SIZE			(128 * 1024)
+
 struct uadk_bd {
 	u8 *src;
 	u8 *dst;
@@ -61,6 +62,7 @@ struct zip_file_head {
 
 static struct wd_ctx_config g_ctx_cfg;
 static struct wd_sched *g_sched;
+static struct sched_params param;
 static unsigned int g_thread_num;
 static unsigned int g_ctxnum;
 static unsigned int g_pktlen;
@@ -240,7 +242,7 @@ static int zip_uadk_param_parse(thread_data *tddata, struct acc_option *options)
 	u8 alg;
 
 	if (optype >= WD_DIR_MAX << 1) {
-		ZIP_TST_PRT("Fail to get zip optype!\n");
+		ZIP_TST_PRT("failed to get zip optype!\n");
 		return -EINVAL;
 	} else if (optype >= WD_DIR_MAX) {
 		mode = STREAM_MODE;
@@ -265,7 +267,7 @@ static int zip_uadk_param_parse(thread_data *tddata, struct acc_option *options)
 		optype = WD_DIR_COMPRESS;
 		break;
 	default:
-		ZIP_TST_PRT("Fail to set zip alg\n");
+		ZIP_TST_PRT("failed to set zip alg\n");
 		return -EINVAL;
 	}
 
@@ -298,21 +300,22 @@ static int init_ctx_config2(struct acc_option *options)
 	/* init */
 	ret = wd_comp_init2(alg_name, SCHED_POLICY_RR, TASK_HW);
 	if (ret) {
-		ZIP_TST_PRT("Fail to do comp init2!\n");
+		ZIP_TST_PRT("failed to do comp init2!\n");
 		return ret;
 	}
 
 	return 0;
 }
 
-static struct sched_params param;
 static int init_ctx_config(struct acc_option *options)
 {
-	struct uacce_dev_list *list;
+	struct uacce_dev_list *list, *tmp;
 	char *alg = options->algclass;
 	int optype = options->optype;
 	int mode = options->syncmode;
-	int i, max_node;
+	struct uacce_dev *dev = NULL;
+	int max_node, i;
+	char *dev_name;
 	int ret = 0;
 
 	optype = optype % WD_DIR_MAX;
@@ -322,61 +325,96 @@ static int init_ctx_config(struct acc_option *options)
 
 	list = wd_get_accel_list(alg);
 	if (!list) {
-		ZIP_TST_PRT("Fail to get %s device\n", alg);
+		ZIP_TST_PRT("failed to get %s device\n", alg);
 		return -ENODEV;
 	}
-	memset(&g_ctx_cfg, 0, sizeof(struct wd_ctx_config));
-	g_ctx_cfg.ctx_num = g_ctxnum;
-	g_ctx_cfg.ctxs = calloc(g_ctxnum, sizeof(struct wd_ctx));
-	if (!g_ctx_cfg.ctxs)
-		return -ENOMEM;
 
-	g_sched = wd_sched_rr_alloc(SCHED_POLICY_RR, 2, max_node, wd_comp_poll_ctx);
-	if (!g_sched) {
-		ZIP_TST_PRT("Fail to alloc sched!\n");
-		goto out;
+	if (strlen(options->device) == 0) {
+		dev = list->dev;
+	} else {
+		for (tmp = list; tmp; tmp = tmp->next) {
+			dev_name = strrchr(tmp->dev->dev_root, '/') + 1;
+			if (!strcmp(dev_name, options->device)) {
+				dev = tmp->dev;
+				break;
+			}
+		}
+	}
+
+	if (dev == NULL) {
+		ZIP_TST_PRT("failed to find device %s\n", options->device);
+		ret = -ENODEV;
+		goto free_list;
 	}
 
 	/* If there is no numa, we defualt config to zero */
-	if (list->dev->numa_id < 0)
-		list->dev->numa_id = 0;
+	if (dev->numa_id < 0)
+		dev->numa_id = 0;
+
+	memset(&g_ctx_cfg, 0, sizeof(struct wd_ctx_config));
+	g_ctx_cfg.ctx_num = g_ctxnum;
+	g_ctx_cfg.ctxs = calloc(g_ctxnum, sizeof(struct wd_ctx));
+	if (!g_ctx_cfg.ctxs) {
+		ret = -ENOMEM;
+		goto free_list;
+	}
 
 	for (i = 0; i < g_ctxnum; i++) {
-		g_ctx_cfg.ctxs[i].ctx = wd_request_ctx(list->dev);
-		g_ctx_cfg.ctxs[i].op_type = optype; // default op_type
+		g_ctx_cfg.ctxs[i].ctx = wd_request_ctx(dev);
+		if (!g_ctx_cfg.ctxs[i].ctx) {
+			ZIP_TST_PRT("failed to alloc %dth ctx\n", i);
+			goto free_ctx;
+		}
+
+		g_ctx_cfg.ctxs[i].op_type = optype;
 		g_ctx_cfg.ctxs[i].ctx_mode = (__u8)mode;
 	}
+
+	g_sched = wd_sched_rr_alloc(SCHED_POLICY_RR, 2, max_node, wd_comp_poll_ctx);
+	if (!g_sched) {
+		ZIP_TST_PRT("failed to alloc sched!\n");
+		ret = -ENOMEM;
+		goto free_ctx;
+	}
+
 	g_sched->name = SCHED_SINGLE;
 
 	/*
 	 * All contexts for 2 modes & 2 types.
 	 * The test only uses one kind of contexts at the same time.
 	 */
-	param.numa_id = list->dev->numa_id;
+	param.numa_id = dev->numa_id;
 	param.type = optype;
 	param.mode = mode;
 	param.begin = 0;
 	param.end = g_ctxnum - 1;
 	ret = wd_sched_rr_instance(g_sched, &param);
 	if (ret) {
-		ZIP_TST_PRT("Fail to fill sched data!\n");
-		goto out;
+		ZIP_TST_PRT("failed to fill sched data!\n");
+		goto free_sched;
 	}
 
-	/* init */
 	ret = wd_comp_init(&g_ctx_cfg, g_sched);
 	if (ret) {
-		ZIP_TST_PRT("Fail to cipher ctx!\n");
-		goto out;
+		ZIP_TST_PRT("failed to cipher ctx!\n");
+		goto free_sched;
 	}
 
 	wd_free_list_accels(list);
 
 	return 0;
-out:
-	free(g_ctx_cfg.ctxs);
+
+free_sched:
 	wd_sched_rr_release(g_sched);
 
+free_ctx:
+	for (; i >= 0; i--)
+		wd_release_ctx(g_ctx_cfg.ctxs[i].ctx);
+	free(g_ctx_cfg.ctxs);
+
+free_list:
+	wd_free_list_accels(list);
+
 	return ret;
 }
 
-- 
2.25.1

