%define soversion 2
Name:          libwd
Summary:       User Space Accelerator Development Kit
Version:       2.6.0
Release:       3
License:       Apache-2.0
Source:        %{name}-%{version}.tar.gz

Vendor:        Huawei Corporation
ExclusiveOS:   linux
URL:           https://support.huawei.com
BuildRoot:     %{_tmppath}/%{name}-%{version}-root
Conflicts:     %{name} < %{version}-%{release}
Provides:      %{name} = %{version}-%{release}
BuildRequires: numactl-devel, compat-openssl11-devel, zlib-devel
BuildRequires: automake, autoconf, libtool, chrpath
BuildRequires: gcc, make
ExclusiveArch: aarch64

Patch01:	0001-uadk-fix-build-issue-of-pthread_atfork.patch
Patch02:	0002-uadk-fix-static-build-error.patch
Patch03:	0003-uadk-add-secure-compilation-option.patch
Patch04:	0004-uadk_tool-fix-build-error.patch
Patch05:	0005-v1-fix-build-error.patch
Patch06:	0006-wd_mempool-fix-build-error.patch
Patch07:	0007-wd_rsa-fix-build-error.patch
Patch08:	0008-test-fix-build-error.patch
Patch0009:	0009-uadk-sec-move-function-to-wd_digest_drv.h.patch
Patch0010:	0010-uadk-digest-add-partial_block-to-store-partial-data.patch
Patch0011:	0011-uadk-digest-add-wd_ctx_spin_lock-function.patch
Patch0012:	0012-uadk-remove-redundant-header-file-in-makefile.patch
Patch0013:	0013-uadk-isa-ce-support-sm3-ce-instruction.patch
Patch0014:	0014-uadk-fix-control-range-of-environmemt-variable.patch
Patch0015:	0015-uadk-util-use-default-sched_type-for-instruction-tas.patch
Patch0016:	0016-uadk-digest-modify-spelling-errors.patch
Patch0017:	0017-uadk-drv-hisi-fix-failed-to-init-drv-after-fork.patch
Patch0018:	0018-wd_rsa-fix-wd_rsa_common_uninit-re-entry.patch
Patch0019:	0019-wd_dh-Fix-wd_aead_uninit-re-entry.patch
Patch0020:	0020-wd_ecc-Fix-wd_ecc_uninit-re-entry.patch
Patch0021:	0021-wd_digest-uninit-check-status-in-one-func.patch
Patch0022:	0022-wd_aead-uninit-check-status-in-one-func.patch
Patch0023:	0023-makefile-install-wd_zlibwrapper.h-to-system.patch
Patch0024:	0024-conf-fix-includedir.patch
Patch0025:	0025-cipher-add-support-for-SM4-CBC-and-CTR-modes-in-CE-i.patch
Patch0026:	0026-cipher-add-support-for-SM4-CFB-and-XTS-modes-in-CE-i.patch
Patch0027:	0027-cipher-add-support-for-SM4-ECB-algorithm-in-CE-instr.patch
Patch0028:	0028-uadk-cipher-isa_ce-support-SM4-cbc_cts-mode.patch
Patch0029:	0029-uadk-wd_alg-check-whether-the-platform-supports-SVE.patch
Patch0030:	0030-uadk-sched-fix-async-mode-ctx-id.patch
Patch0031:	0031-uadk-initializes-ctx-resources-in-SVE-mode.patch
Patch0032:	0032-uadk-hash_mb-support-multi-buffer-calculation-for-sm.patch
Patch0033:	0033-uadk_tool-fix-aead-performance-test-issue.patch
Patch0034:	0034-uadk_tool-fix-the-logic-for-counting-retransmissions.patch
Patch0035:	0035-uadk-tools-support-the-nosva-test-of-a-specified-dev.patch
Patch0036:	0036-uadk-tools-support-designated-device-testing.patch
Patch0037:	0037-uadk_tool-support-sm3-ce-benchmark-and-function-test.patch
Patch0038:	0038-uadk_tool-support-sm4-ce-benchmark-test.patch
Patch0039:	0039-uadk_tool-support-sm3-md5-multibuff-benchmark-test.patch
Patch0040:	0040-uadk-tool-fix-the-msg-pool-release-bug-of-async-zip-.patch
Patch0041:	0041-uadk_tool-fix-queue-application-failure-from-multipl.patch
Patch0042:	0042-ecc-check-need_debug-before-calling-WD_DEBUG.patch
Patch0043:	0043-uadk-remove-unused-ioctl-cmd.patch
Patch0044:	0044-uadk-v1-remove-dummy.patch
Patch0045:	0045-cipher-optimze-input-lengths-check.patch
Patch0046:	0046-uadk-v1-improve-the-judgment-conditions-of-tag.patch
Patch0047:	0047-uadk-v1-fix-for-sec-cipher-bd1-ci_gen-configuration.patch
Patch0048:	0048-uadk-fix-for-shmget-shmflag.patch
Patch0049:	0049-sec-optimze-for-directly-assigning-values-to-structu.patch
Patch0050:	0050-util-optimize-for-wd_handle_msg_sync.patch
Patch0051:	0051-uadk-drv_hisi-optimize-qm-recv-function.patch
Patch0052:	0052-uadk-modify-uadk-static-compile.patch

%description
This package contains the User Space Accelerator Library
for hardware accelerator, compress, symmetric encryption
and decryption, asymmetric encryption and decryption.

%global debug_package %{nil}
%prep
%autosetup -n %{name}-%{version} -p1

%build
sh autogen.sh
./configure
make

%install
mkdir -p ${RPM_BUILD_ROOT}%{_libdir}/uadk
install -b -m755 .libs/libwd*.so.%{version} ${RPM_BUILD_ROOT}%{_libdir}
install -b -m755 .libs/libhisi_*.so.%{version} ${RPM_BUILD_ROOT}%{_libdir}/uadk

# create symbolic link
for lib in $RPM_BUILD_ROOT%{_libdir}/*.so.%{version} ; do
	ln -s -f `basename ${lib}` $RPM_BUILD_ROOT%{_libdir}/`basename ${lib} .%{version}`
	ln -s -f `basename ${lib}` $RPM_BUILD_ROOT%{_libdir}/`basename ${lib} .%{version}`.%{soversion}
done
for lib in $RPM_BUILD_ROOT%{_libdir}/uadk/*.so.%{version} ; do
 	ln -s -f `basename ${lib}` $RPM_BUILD_ROOT%{_libdir}/uadk/`basename ${lib} .%{version}`
 	ln -s -f `basename ${lib}` $RPM_BUILD_ROOT%{_libdir}/uadk/`basename ${lib} .%{version}`.%{soversion}
done

chrpath -d ${RPM_BUILD_ROOT}%{_libdir}/libwd*.so.%{version}
chrpath -d ${RPM_BUILD_ROOT}%{_libdir}/uadk/libhisi_*.so.%{version}

mkdir -p ${RPM_BUILD_ROOT}%{_includedir}/warpdrive/include
cp v1/uacce.h ${RPM_BUILD_ROOT}%{_includedir}/warpdrive/include
cp v1/*.h ${RPM_BUILD_ROOT}%{_includedir}/warpdrive

mkdir -p ${RPM_BUILD_ROOT}%{_includedir}/uadk/v1
cp include/*.h ${RPM_BUILD_ROOT}%{_includedir}/uadk
cp v1/*.h ${RPM_BUILD_ROOT}%{_includedir}/uadk/v1

mkdir -p ${RPM_BUILD_ROOT}%{_libdir}/pkgconfig
cp lib/*.pc ${RPM_BUILD_ROOT}%{_libdir}/pkgconfig


%clean
rm -rf ${RPM_BUILD_ROOT}

%files
%defattr(755,root,root)
%{_libdir}/libwd*.so.%{version}
%{_libdir}/libwd*.so
%{_libdir}/libwd*.so.%{soversion}
%{_libdir}/uadk/libhisi_*.so.%{version}
%{_libdir}/uadk/libhisi_*.so
%{_libdir}/uadk/libhisi_*.so.%{soversion}
%defattr(644,root,root)
%{_includedir}/warpdrive/include/uacce.h
%{_includedir}/warpdrive/*.h
%{_includedir}/uadk/*.h
%{_includedir}/uadk/v1/*.h
%{_libdir}/pkgconfig/*.pc
%exclude %{_includedir}/warpdrive/uacce.h
%exclude %{_includedir}/warpdrive/wd_util.h
%exclude %{_includedir}/warpdrive/wd_adapter.h
%exclude %{_includedir}/uadk/wd_util.h
%exclude %{_includedir}/uadk/hisi_qm_udrv.h
%exclude %{_includedir}/uadk/v1/wd_util.h
%exclude %{_includedir}/uadk/v1/wd_adapter.h

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%changelog
* Sun Apr 7 2024 JiangShui Yang <yangjiangshui@h-partners.com> 2.6.0-3
- libwd: update the source code

* Thu Feb 22 2024 JiangShui Yang <yangjiangshui@h-partners.com> 2.6.0-2
- libwd: simplify warpdrive.spec

* Mon Jan 22 2024 Zhangfei Gao <zhangfei.gao@linaro.org> 2.6.0-1
- libwd: update to 2.6.0

* Mon Sep 18 2023 renyi <977713017@qq.com> 2.5.0-4
- support clang compile

* Mon Sep 4 2023 JiangShui Yang <yangjiangshui@h-partners.com> 2.5.0-3
- libwd: Deleting rpath information from the dynamic library

* Wed Jun 14 2023 JiangShui Yang <yangjiangshui@h-partners.com> 2.5.0-2
- libwd: Adding and packing the pkgconfig file

* Wed Jun 14 2023 JiangShui Yang <yangjiangshui@h-partners.com> 2.5.0-1
- libwd: update the source code to 2.5.0

* Thu Jun 8 2023 JiangShui Yang <yangjiangshui@h-partners.com> 2.4.0-1
- libwd: update the source code to 2.4.0

* Mon Mar 20 2023 JiangShui Yang <yangjiangshui@h-partners.com> 2.3.37-4
- libwd: modify the build requirement

* Wed Feb 15 2023 Yang Shen <shenyang39@huawei.com> 2.3.37-3
- libwd: add build requirement

* Thu Dec 15 2022 Yang Shen <shenyang39@huawei.com> 2.3.37-2
- libwd: fix a bug for ecc

* Mon Oct 10 2022 Yang Shen <shenyang39@huawei.com> 2.3.37-1
- libwd: update the source code to 2.3.37

* Tue Jul 26 2022 Yang Shen <shenyang39@huawei.com> 2.3.21-6
- libwd: backport the patch of uadk from 2.3.31 to 2.3.36

* Mon Mar 21 2022 Yang Shen <shenyang39@huawei.com> 2.3.21-5
- libwd: backport the patch of uadk from 2.3.28 to 2.3.31

* Tue Mar 1 2022 Yang Shen <shenyang39@huawei.com> 2.3.21-4
- libwd: backport the patch of uadk from 2.3.27 to 2.3.28

* Mon Feb 21 2022 Yang Shen <shenyang39@huawei.com> 2.3.21-3
- libwd: backport the patch of uadk from 2.3.24 to 2.3.27

* Tue Jan 04 2022 Yang Shen <shenyang39@huawei.com> 2.3.21-2
- libwd: backport the patch of uadk from 2.3.21 to 2.3.24

* Mon Dec 06 2021 linwenkai <linwenkai6@hisilicon.com> 2.3.21-1
- libwd: update uadk from 2.3.20 to 2.3.21

* Wed Nov 24 2021 linwenkai <linwenkai6@hisilicon.com> 2.3.20-2
- libwd: add missing head files and fix install path

* Mon Nov 22 2021 Yang Shen <shenyang39@huawei.com> 2.3.20-1
- libwd: update uadk from 2.3.11 to 2.3.20

* Fri Sep 03 2021 Yang Shen <shenyang39@huawei.com> 2.3.11-4
- uadk-access-the-file-isolate-before-read-it.patch

* Fri Aug 06 2021 Pengju Jiang <jiangpengju2@huawei.com> 2.3.11-3
- bugfix-of-gcc-10.patch

* Wed Jul 21 2021 caodongxia <caodongxia@huawei.com> 2.3.11-2
- change uacce.h path.

* Tue Jul 06 2021 Hao Fang <fanghao11@huawei.com> 2.3.11-1
- update warpdrive to uadk, support kunpeng 920/930.

* Sun Mar 15 2020 zhangtao <zhangtao221@huawei.com> 1.2.10-3
- Specify compilation aarch64 as platform

* Tue Mar 03 2020 catastrowings <jianghuhao1994@163.com> 1.2.10-2
- openEuler init

* Tue Jan 07 2020 jinbinhua <jinbinhua@huawei.com> 1.2.7-1
- First Spec Version Include Warpdrive Code
