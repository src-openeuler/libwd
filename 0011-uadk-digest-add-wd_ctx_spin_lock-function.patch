From b06161de909136e59ecd7f148ef7e8ba72652e34 Mon Sep 17 00:00:00 2001
From: Weili Qian <qianweili@huawei.com>
Date: Mon, 5 Feb 2024 17:27:17 +0800
Subject: [PATCH 11/44] uadk/digest: add wd_ctx_spin_lock function

In synchronous mode, to protect hardware queue resources and
prevent multiple threads from sending packets to the same queue
at the same time, lock is added before packets are sent in function
send_recv_sync().

In non-hard computing scenarios, the resources are independent,
and multiple synchronization threads can process at the same time.
If lock is added before packets are sent, the multi-thread performance
deteriorates. Therefore, the wd_ctx_spin_lock and wd_ctx_spin_unlock
interfaces are added. In non-hard computing scenarios, the lock
is not added.

Signed-off-by: Weili Qian <qianweili@huawei.com>
---
 include/wd_util.h | 23 +++++++++++++++++++++++
 wd_digest.c       |  4 ++--
 2 files changed, 25 insertions(+), 2 deletions(-)

diff --git a/include/wd_util.h b/include/wd_util.h
index 3059ac1..f217f0f 100644
--- a/include/wd_util.h
+++ b/include/wd_util.h
@@ -527,6 +527,29 @@ static inline void wd_dfx_msg_cnt(struct wd_ctx_config_internal *config,
 	config->msg_cnt[sqn]++;
 }
 
+/**
+ * wd_ctx_spin_lock() - Lock interface, which is used in the synchronization process.
+ * @ctx: queue context.
+ * @type: the type of the driver.
+ *
+ * If the drvier type is not UADK_ALG_HW, the lock is not required.
+ */
+static inline void wd_ctx_spin_lock(struct wd_ctx_internal *ctx, int type)
+{
+	if (type != UADK_ALG_HW)
+		return;
+
+	pthread_spin_lock(&ctx->lock);
+}
+
+static inline void wd_ctx_spin_unlock(struct wd_ctx_internal *ctx, int type)
+{
+	if (type != UADK_ALG_HW)
+		return;
+
+	pthread_spin_unlock(&ctx->lock);
+}
+
 #ifdef __cplusplus
 }
 #endif
diff --git a/wd_digest.c b/wd_digest.c
index dba2f95..c59184d 100644
--- a/wd_digest.c
+++ b/wd_digest.c
@@ -581,10 +581,10 @@ static int send_recv_sync(struct wd_ctx_internal *ctx, struct wd_digest_sess *ds
 	msg_handle.send = wd_digest_setting.driver->send;
 	msg_handle.recv = wd_digest_setting.driver->recv;
 
-	pthread_spin_lock(&ctx->lock);
+	wd_ctx_spin_lock(ctx, wd_digest_setting.driver->calc_type);
 	ret = wd_handle_msg_sync(wd_digest_setting.driver, &msg_handle, ctx->ctx,
 				 msg, NULL, wd_digest_setting.config.epoll_en);
-	pthread_spin_unlock(&ctx->lock);
+	wd_ctx_spin_unlock(ctx, wd_digest_setting.driver->calc_type);
 	if (unlikely(ret))
 		return ret;
 
-- 
2.25.1

